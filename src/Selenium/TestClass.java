package Selenium;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class TestClass {
    public static void main(String[] args) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver","/home/nandhukumar/Downloads/chromedriver");

        WebDriver runDriver = new ChromeDriver();

        runDriver.manage().window().maximize();

        runDriver.get("https://meesho.com/");


        hoverOverMenus(runDriver);
        scrollDownAndUp(runDriver);
        searchProduct(runDriver);
        addToCart(runDriver);
        signUpPage(runDriver);
    }

    public static void hoverOverMenus(WebDriver runDriver) throws InterruptedException {
        WebElement womenEthnic = runDriver.findElement(By.xpath("//*[@id=\"__next\"]/div[2]/div[3]/div/div[1]"));
        WebElement womenWestern = runDriver.findElement(By.xpath("//*[@id=\"__next\"]/div[2]/div[3]/div/div[3]"));
        WebElement jewelleryAndAccessory = runDriver.findElement(By.xpath("//*[@id=\"__next\"]/div[2]/div[3]/div/div[5]"));
        WebElement men = runDriver.findElement(By.xpath("//*[@id=\"__next\"]/div[2]/div[3]/div/div[7]"));
        WebElement beautyAndHealth = runDriver.findElement(By.xpath("//*[@id=\"__next\"]/div[2]/div[3]/div/div[9]"));
        WebElement bagsAndFootwear = runDriver.findElement(By.xpath("//*[@id=\"__next\"]/div[2]/div[3]/div/div[11]"));
        WebElement homeAndKitchen = runDriver.findElement(By.xpath("//*[@id=\"__next\"]/div[2]/div[3]/div/div[13]"));
        WebElement kids = runDriver.findElement(By.xpath("//*[@id=\"__next\"]/div[2]/div[3]/div/div[15]"));
        WebElement electronics = runDriver.findElement(By.xpath("//*[@id=\"__next\"]/div[2]/div[3]/div/div[17]"));
        WebElement container = runDriver.findElement(By.id("__next"));


        Actions actions = new Actions(runDriver);

        actions.moveToElement(womenEthnic).perform();
        Thread.sleep(1000);

        actions.moveToElement(womenWestern).perform();
        Thread.sleep(1000);

        actions.moveToElement(jewelleryAndAccessory).perform();
        Thread.sleep(1000);

        actions.moveToElement(men).perform();
        Thread.sleep(1000);

        actions.moveToElement(beautyAndHealth).perform();
        Thread.sleep(1000);

        actions.moveToElement(bagsAndFootwear).perform();
        Thread.sleep(1000);

        actions.moveToElement(homeAndKitchen).perform();
        Thread.sleep(1000);

        actions.moveToElement(kids).perform();
        Thread.sleep(1000);

        actions.moveToElement(electronics).perform();
        Thread.sleep(1000);

        actions.moveToElement(container).perform();
        Thread.sleep(1000);
    }

    public static void scrollDownAndUp(WebDriver runDriver) throws InterruptedException {
        JavascriptExecutor js = (JavascriptExecutor) runDriver;
        js.executeScript("window.scrollBy(0,250)", "");
        Thread.sleep(1000);

        js.executeScript("window.scrollBy(0,-200)", "");
        Thread.sleep(1000);

        js.executeScript("window.scrollBy(0,500)", "");
        Thread.sleep(1000);

        js.executeScript("window.scrollBy(0,-550)", "");
        Thread.sleep(1000);

    }

    public static void searchProduct(WebDriver runDriver) throws InterruptedException {

        WebElement searchBar = runDriver.findElement(By.xpath("//*[@id=\"__next\"]/div[2]/div[1]/div/div[2]/div/input"));

        searchBar.sendKeys("toys");

        Thread.sleep(1000);

        searchBar.sendKeys(Keys.ENTER);

        Thread.sleep(2000);

        WebElement kidsToy = runDriver.findElement(By.xpath("/html/body/div/div[3]/div/div[4]/div/div[3]/a/div/div[2]/p"));

        kidsToy.click();
    }

    public static void addToCart(WebDriver runDriver) throws InterruptedException {

        Thread.sleep(1000);

        WebElement addToCartButton = runDriver.findElement(By.xpath("//*[@id=\"__next\"]/div[3]/div/div[1]/div/div[2]/div[2]/button"));

        addToCartButton.click();
    }

    public static void signUpPage(WebDriver runDriver) throws InterruptedException {

        Thread.sleep(1000);

        WebElement inputNumber =  runDriver.findElement(By.xpath("//*[@id=\"__next\"]/div[3]/div/div[2]/div/div/div[2]/input"));

        inputNumber.sendKeys("9994429999");

        Thread.sleep(1000);

        WebElement sendOTP = runDriver.findElement(By.xpath("//*[@id=\"__next\"]/div[3]/div/div[2]/div/button"));

        sendOTP.click();
    }

}
